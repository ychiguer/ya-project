use std::{time::{SystemTime}, fs::File};

use clap::{Command, ArgMatches, Arg};

use serde_json::Value;

use state_box::{LogState, StateBox, StateEnum};



pub fn command() -> Command<'static> {
    Command::new("log")
        .about("ya logger subcommand")
        .subcommand_required(true)
        .subcommand(common_args(Command::new("info"))
        )
        .subcommand(common_args(Command::new("enter"))
        )
        .subcommand(common_args(Command::new("exit"))
        )
}

fn common_args(cmd: Command<'static>) -> Command<'static> {

    let json_schema: Value = serde_json::from_reader(File::open("./ya_log_state.schema.json").unwrap()).unwrap();
    let properties = json_schema.get("properties").unwrap().as_object().unwrap().to_owned().into_iter();

    cmd.args(
        properties.map(|e| {
            let k: &str = Box::leak(e.0.to_owned().into_boxed_str());
            let value: &str = Box::leak(e.1.as_str().unwrap_or_default().to_owned().into_boxed_str());
             Arg::new(k)
                .long(k) 
                .takes_value(true)
                .required(true)
                .default_value(value)
        })
    )
    
}

pub fn matches(matches: &ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    match matches.subcommand_matches("log") {
        Some(m) => {
            if let Some(args) = m.subcommand_matches("info") {
                   return common(args,'.');
            }
            else if let Some(args)= m.subcommand_matches("enter") {
                return common(args,'>');
            } else  if let Some(args)= m.subcommand_matches("exit") {
                return common(args,'<');
            }
            return Ok(None) ;
        }, 
        None => {Ok(None)},
    }
}
// If there is many args to be made into a list we have to use a plural form for the argument.
fn common(m: &ArgMatches, dif:char) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let t: &String = m.get_one("time").unwrap();
    let s: &String = m.get_one("source").unwrap();
    let l: &String = m.get_one("line").unwrap();
    let p: &String = m.get_one("pid").unwrap();
    let pp: &String = m.get_one("ppid").unwrap();
    let m1: &String = m.get_one("messageKey1").unwrap();
    let m2: &String = m.get_one("messageBigLongKey2").unwrap();

    let mut key1 = "messageKey1".to_string();
    let key2 = "messageBigLongKey2";
    (0..(key2.len()-key1.len())).enumerate().for_each(|_| key1.push(' '));

    let now:u64;
    let time = t.parse().unwrap_or_default();
    if SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() >= time {
        now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() - time;
    } else {
        return Ok(Some("Please provide a valid time".to_string()))
    }
    let hour = now/3600 as u64;
    let min = (now-  hour*3600)/60 as u64;
    let sec = now- hour*3600 - min*60;

    let exec: &str;
    if m.get_one::<bool>("local").copied().unwrap() {
        exec = "Local";
    } else {
        exec = "Remote";
    }

    Ok(Some(format!("[{}:{}:{}][{}:{}] ({}->{}) {} {} {}\n[{}:{}:{}][{}:{}] ({}->{}) {} {} {} >>> {}",hour,min,sec,s,l,p,pp,dif,key1,m1,hour,min,sec,s,l,p,pp,dif,key2,m2,exec)))
}

pub fn structy(m: &ArgMatches) -> Result<Option<StateBox>, Box<dyn std::error::Error>> {
    match m.subcommand_matches("log") {
        Some(_m) => {
            return Ok(logic(_m));
        }, 
        None => {Ok(None)},
    }
}

pub fn state(m: &ArgMatches) -> Result<Option<Vec<Vec<String>>>, Box<dyn std::error::Error>> {
    match m.subcommand_matches("log") {
        Some(_m) => {
            let matche = logic(_m);
            let path = "./log_state.json";
            if File::open(path).is_ok() {
                let file = File::open(path).unwrap();
                let state: LogState = serde_json::from_reader(file).unwrap();
                let (_t,_s,_l,_p,_pp, _m1,_m2) = (state.time,state.source,state.line,state.pid,state.ppid,state.message_key1,state.message_big_long_key2);
            
                if matche.is_some() {
                    let cli_state = match matche.unwrap().state {
                        StateEnum::LogState(s) => {s},
                        _ => {panic!("Wrong subcommand")}
                    };
                    let (t,s,l,p,pp, m1,m2) = (cli_state.time,cli_state.source,cli_state.line,cli_state.pid,cli_state.ppid,cli_state.message_key1,cli_state.message_big_long_key2);
                    return Ok(Some(vec![
                        vec!["time".to_string(), format!("{}{}{}",'"',t.to_string(),'"'), format!("{}{}{}",'"',_t.to_string(),'"')],
                        vec!["source".to_string(), format!("{}{}{}",'"',s.to_string(),'"'), format!("{}{}{}",'"',_s.to_string(),'"')],
                        vec!["line".to_string(), format!("{}{}{}",'"',l.to_string(),'"'), format!("{}{}{}",'"',_l.to_string(),'"')],
                        vec!["pid".to_string(), format!("{}{}{}",'"',p.to_string(),'"'), format!("{}{}{}",'"',_p.to_string(),'"')],
                        vec!["ppid".to_string(), format!("{}{}{}",'"',pp.to_string(),'"'), format!("{}{}{}",'"',_pp.to_string(),'"')],
                        vec!["messageKey1".to_string(), format!("{}{}{}",'"',m1.to_string(),'"'), format!("{}{}{}",'"',_m1.to_string(),'"')],
                        vec!["messageBigLongKey2".to_string(), format!("{}{}{}",'"',m2.to_string(),'"'), format!("{}{}{}",'"',_m2.to_string(),'"')], 
                        // Using format to ensure the string nature of the argument
                    ]))
                } else {
                    return Ok(Some(vec![
                        vec!["time".to_string(), "".to_string(), format!("{}{}{}",'"',_t.to_string(),'"')],
                        vec!["source".to_string(), "".to_string(), format!("{}{}{}",'"',_s.to_string(),'"')],
                        vec!["line".to_string(), "".to_string(), format!("{}{}{}",'"',_l.to_string(),'"')],
                        vec!["pid".to_string(), "".to_string(), format!("{}{}{}",'"',_p.to_string(),'"')],
                        vec!["ppid".to_string(), "".to_string(), format!("{}{}{}",'"',_pp.to_string(),'"')],
                        vec!["messageKey1".to_string(), "".to_string(), format!("{}{}{}",'"',_m1.to_string(),'"')],
                        vec!["messageBigLongKey2".to_string(), "".to_string(), format!("{}{}{}",'"',_m2.to_string(),'"')], 
                    ]))
                }
            }
            return Ok(Some(vec![
                vec!["time".to_string(), "".to_string(), "".to_string()],
                vec!["source".to_string(), "".to_string(), "".to_string()],
                vec!["line".to_string(), "".to_string(), "".to_string()],
                vec!["pid".to_string(), "".to_string(), "".to_string()],
                vec!["ppid".to_string(), "".to_string(), "".to_string()],
                vec!["messageKey1".to_string(), "".to_string(), "".to_string()],
                vec!["messageBigLongKey2".to_string(), "".to_string(), "".to_string()], 
            ]))
        }, 
        None => {Ok(None)},
    }
}

fn logic (m: &ArgMatches) -> Option<StateBox> {
        if m.subcommand_matches("info").is_some() || m.subcommand_matches("enter").is_some() || m.subcommand_matches("exit").is_some() {
            let _m;
            if m.subcommand_matches("info").is_some() {
                _m = m.subcommand_matches("info").unwrap()
            } else if m.subcommand_matches("enter").is_some() {
                _m = m.subcommand_matches("enter").unwrap()
            } else {
                _m = m.subcommand_matches("exit").unwrap()
            }
            let args = (_m.get_many("time"),_m.get_many("source"),_m.get_many("line"),_m.get_many("pid"),_m.get_many("ppid"),_m.get_many("messageKey1"),_m.get_many("messageBigLongKey2"));
            let (t,s,l,p,pp, m1,m2): (&String,&String,&String,&String,&String,&String,&String) = match args
            {
                (Some(_t),Some(_s),Some(_l),Some(_p),Some(_pp),Some(_m1),Some(_m2)) => match (_t.last(),_s.last(),_l.last(),_p.last(),_pp.last(),_m1.last(),_m2.last()) {
                    (Some(__t),Some(__s),Some(__l),Some(__p),Some(__pp),Some(__m1),Some(__m2)) => (__t,__s,__l,__p,__pp,__m1,__m2),
                    _ =>  {return None}
                },
                _ => {return None} 
            };
            let result = StateBox {udomid:"ya_echos".to_string(), state: StateEnum::LogState(LogState {
                time: t.parse().unwrap_or_default(),
                source: s.to_owned(),
                line: l.parse().unwrap_or_default(),
                pid: p.parse().unwrap_or_default(),
                ppid: pp.parse().unwrap_or_default(),
                message_key1: m1.to_owned(),
                message_big_long_key2: m2.to_string()
            }) };
            return Some(result);
    } else {
        return None;
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_logger(bencher: &mut Bencher) {
        let matches = command().get_matches_from(vec!["log", "info", "-t", "82374686", "-l", "5", "-s", "test.rs", "-p", "3245", "-P", "2344", "--messageKey1", "Hello", "--messageBigLongKey2", "hey there"]);
        bencher.iter(|| common(&matches, '.'));
    }
}