use clap::{Command, ArgMatches, Arg};

use state_box::{StateBox};

// subcommand modules
pub mod log;
mod issue;
mod issue_labels;
pub mod echos;

// Main Subcommands enumeration
enum SubCom {
    Log,
    Issue,
    IssueLabels,
    Echos,
}

// From<> implementation to their specific match resolving
// functions.
impl From<&SubCom> for &dyn Fn (&ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> {
    fn from(sc: &SubCom) -> Self {
        match sc {
            SubCom::Log => &log::matches,
            SubCom::Issue => &issue::matches,
            SubCom::IssueLabels => &issue_labels::matches,
            SubCom::Echos => &echos::matches,
        }
    }
}

fn to_structy(m: &ArgMatches, sc: &SubCom) -> Result<Option<StateBox>, Box<dyn std::error::Error>> {
        match sc {
            SubCom::Echos => echos::structy(m),
            SubCom::Log => log::structy(m),
            SubCom::Issue | SubCom::IssueLabels => todo!(),
        }
}

impl From<&SubCom> for &dyn Fn (&ArgMatches) -> Result<Option<Vec<Vec<String>>>, Box<dyn std::error::Error>> {
    fn from(sc: &SubCom) -> Self {
        match sc {
            SubCom::Echos => &echos::state,
            SubCom::Log => &log::state,
            SubCom::Issue => todo!(),
            SubCom::IssueLabels => todo!(),
        }
    }
}

fn get_coms() -> Vec<SubCom> {
    vec![SubCom::Log, SubCom::Issue, SubCom::IssueLabels, SubCom::Echos]
}



pub fn get_cli() -> Command<'static> {
    
    // Main interface for the CLI
    // @note: All subcommands have to be added here
    // then have their module for match resolving and
    // specifing +2 subcommands
    Command::new("ya")
        .version("1.0")
        .about("CLI for Bashroom workflow.")
        .author("Bashroom.com")
        .subcommand_required(true)
        .subcommand(
            echos::command()
            )
        // Logger subcommand
        .subcommand(
            log::command()
            )
        // Issue subcommand
        .subcommand(
            issue::command()
            )
        // Issue lables subcommand
        .subcommand(
            issue_labels::command()
            ).ignore_errors(true)
            .disable_help_flag(true)
        .arg(Arg::new("local")
            .action(clap::ArgAction::SetTrue)
            .long("local") 
            .help("Execute command localy. By default commands are executed remotely")
            .global(true))
}

pub fn subcom_matche(cli_args: &ArgMatches) -> Result<String, Box<dyn std::error::Error>>{

    // Vector of available 
    let sub_commands = get_coms();

    // Matching for subcommands
    match sub_commands.iter().map(|cmd| {
        
            let fun: &dyn Fn (&ArgMatches) -> Result<Option<String>, Box<dyn std::error::Error>> = cmd.into();
            fun(cli_args)
        }).find(|s| if let Ok(smt) = s {smt.is_some()} else {true}) {
        Some(s) => {match s {
            Ok(Some(st)) => {Ok(st)},
            Err(e) => Err(e),
            Ok(None) => { // Only to mute non-exhaustif matching error but already dealt with in find closure
                Ok("Error in matching your arguments!".to_string())
            }
        }},
        None => {Ok("Error in matching your arguments!".to_string())},
    }
}

pub fn structy(cli_args: &ArgMatches) -> Option<StateBox> {
    // Vector of available 
    let sub_commands = vec![SubCom::Log,SubCom::Echos];
    // Matching for subcommands
    match sub_commands.iter().map(|cmd| {
        to_structy(cli_args,cmd)
        }).find(|s| if let Ok(smt) = s {smt.is_some()} else {false}) {
        Some(s) => {match s {
            Ok(Some(st)) => Some(st),
            _ => None
        }},
        None => None,
    }
}

pub fn state(cli_args: &ArgMatches) -> Vec<Vec<String>> {
    // Vector of available 
    let sub_commands = vec![SubCom::Log,SubCom::Echos];
    // Matching for subcommands
    match sub_commands.iter().map(|cmd| {
        
            let fun: &dyn Fn (&ArgMatches) -> Result<Option<Vec<Vec<String>>>, Box<dyn std::error::Error>> = cmd.into();
            fun(cli_args)
        }).find(|s| if let Ok(smt) = s {smt.is_some()} else {false}) {
        Some(s) => {match s {
            Ok(Some(st)) => {st},
            _ => vec![]
        }},
        None => vec![]
    }
}