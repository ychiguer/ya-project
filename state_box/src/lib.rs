use typify::import_types;
use serde::{Deserialize, Serialize};

import_types!(
    schema = "../ya_log_state.schema.json",
    derives = [],
);

import_types!(
    schema = "../ya_echos_state.schema.json",
    derives = [],
);

// >>> import json schema

#[derive(Clone)]
pub enum StateEnum {
    LogState(LogState),
    EchosState(EchosState),
    // >>> enum field construction
}


pub struct StateBox {
    pub udomid: String,
    pub state: StateEnum,
}